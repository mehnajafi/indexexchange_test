import sqlite3

from datetime import date, timedelta

import collections

#run select queries
def RunQuery(db, query, args=None):
    '''(str, str, [tuple]) -> list of tuple
    Return the results of running the given query on database db.'''
    
    con =  sqlite3.connect(db)
    cur =  con.cursor()
    if args == None:
        cur.execute(query)
    else:
        cur.execute(query, args)
    data = cur.fetchall()
    cur.close()
    con.close()
    return data
    
# insert, update, create table commands
def RunCommand(db, command, args):
    '''(str, str, [tuple]) -> NoneType
    Execute the given command with the args on database db.'''
    
    con =  sqlite3.connect(db)
    cur =  con.cursor()
    if args == None:
        cur.execute(command)
    else:
        cur.execute(command, args)
    cur.close()
    con.commit()
    con.close()
    
#creates tables mailing and address
def SetupTables (db):
    RunCommand(db, 'CREATE TABLE mailing (addr VARCHAR(255) NOT NULL);', None)

    RunCommand(db, 'CREATE TABLE domain (dom VARCHAR(50) NOT NULL, count BIGINT NOT NULL, entry_date DATE NOT NULL);', None)
    
#inserts a record into table address
#and updates table domain
def AddNewAddr(db, addr):
    RunCommand(db, 'INSERT INTO mailing VALUES (?)', (addr,))
    
    dom_index = addr.find('@')
    #print dom_index
    
    if (dom_index > -1):
        dom = addr[(dom_index + 1): (len(addr) - 1)]
        print dom
       
        dom_count = RunQuery(db, 'SELECT count FROM domain WHERE dom = (?) AND entry_date = (?)', (dom, date.today()))
        #print dom_count
    
        if(dom_count <> []):
            doms_count = dom_count[0]
            doms_count1 = doms_count[0]
            RunCommand(db, 'UPDATE domain SET count = (?) WHERE dom = (?) AND entry_date = (?)', 
                (doms_count1 + 1, dom, date.today()))
        else:
            RunCommand(db, 'INSERT INTO domain VALUES (?, ?, ?)', (dom, 1, date.today()))
        
#inserts email addresses into database
def InsertAddrs(db, data_file):
    f = open(data_file, 'r')
    
    for addr in f:
        AddNewAddr(db, addr)
        
def GetReport(db):
    begin_date = date.today() - timedelta(30)
    
    domain_growth = {}
    
    print 'begin checking each domain'

    all_domains = RunQuery(db, 'SELECT dom FROM domain')
    
    for dom_object in all_domains:
        print 'begin date'
        # begin date
        dom_count_begin = RunQuery(db, 'SELECT count FROM domain WHERE dom = (?) AND entry_date = (?)', 
                                    (dom_object[0], begin_date))
                               
        print dom_count_begin     
        if (len(dom_count_begin) == 0):
            domc_begin = 0
        else:
            temp = dom_count_begin[0]
            domc_begin = temp[0]
        
        
        print 'current date'
        # current date
        dom_count_today = RunQuery(db, 'SELECT count FROM domain WHERE dom = (?) AND entry_date = (?)', 
                                    (dom_object[0], date.today()))
           
                       
        if (len(dom_count_today) == 0):
            domc_today = 0
        else:
            temp = dom_count_today[0]
            domc_today = temp[0]
            
        #percentage of growth for past 30 days
        if domc_begin == 0: 
            percentage_growth = -1
        else: 
            percentage_growth = ((domc_today - domc_begin)/ float(domc_begin)) * 100 
        
        #first entry date
        print 'first_entry_date'
        first_entry_date = RunQuery(db, 'SELECT MIN(entry_date) FROM domain')
        first_entry_datec = first_entry_date[0]
        dom_count_first = RunQuery(db, 'SELECT count FROM domain WHERE dom = (?) AND entry_date = (?)', 
                                    (dom_object[0], first_entry_datec[0]))
                                    
        print 'percentage of growth for total'
        dom_count_firstc = dom_count_first[0]
        percentage_growth_total = ((domc_today - dom_count_firstc[0])/ float(dom_count_firstc[0])) * 100 
        
        if(percentage_growth == -1):
            domain_growth[dom_object[0]] = -1
        else:
            if (percentage_growth_total > 0):
                domain_growth[dom_object[0]] = percentage_growth / percentage_growth_total
            else:
                domain_growth[dom_object[0]] = -1
            
        domain_growth = collections.OrderedDict(sorted(domain_growth.items()))
        
        report = {}
        i = 0
        for key in domain_growth.keys():
            print 'key = ' + str(key)
            if i >= 50:
                break
            i = i + 1
            
        
        
if __name__ == '__main__':
    #creates tables
    SetupTables('db57.db')
    
    print 'tables are created'
    
    #inserts addresses into database
    InsertAddrs('db57.db', 'data.txt')
    
    print 'addresses are inserted into database'
    
    #print RunQuery('db36.db', 'SELECT * FROM domain')
    
    #reports top 50 domains
    GetReport('db57.db')
    
    print 'done'
    
    